# RVM bootstrap
#$:.unshift(File.expand_path('./lib', ENV['rvm_path']))
require 'rvm/capistrano'
set :rvm_type, :system
set :rvm_ruby_string, '1.9.2'

# bundler bootstrap
require 'bundler/capistrano'

# main details
set :application, "captainsmiley.dyndns.org"
role :web, "captainsmiley.dyndns.org"
role :app, "captainsmiley.dyndns.org"
role :db,  "captainsmiley.dyndns.org", :primary => true

# server details
default_run_options[:pty] = true
ssh_options[:forward_agent] = true
set :deploy_to, "/var/www/captainsmiley.dyndns.org"
set :deploy_via, :remote_cache
set :user, "passenger"
set :use_sudo, false

# repo details
set :scm, :git
set :scm_username, "passenger"
set :repository, "git@personalid:captainsmiley/captainsmiley.dyndns.org.git"
set :branch, "master"
set :git_enable_submodules, 1

# tasks
namespace :deploy do
  task :start, :roles => :app do
    run "touch #{current_path}/tmp/restart.txt"
  end

  task :stop, :roles => :app do
    # Do nothing.
  end

  desc "Restart Application"
  task :restart, :roles => :app do
    run "touch #{current_path}/tmp/restart.txt"
  end
end
load 'deploy/assets'

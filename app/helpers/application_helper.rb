module ApplicationHelper
  def full_title(page_title)
    base_title = "Captainsmiley "
    if page_title.empty?
      base_title + "server"
    else
      custom_title = "#{base_title}" + "| #{page_title}"
        custom_title
    end
  end
end
